﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTarefas.Models
{
    public class Tarefa
    {
        public string identificador { get; set; }
        public int tempo_de_execucao { get; set; }
        public int custo_por_hora { get; set; }
    }
}