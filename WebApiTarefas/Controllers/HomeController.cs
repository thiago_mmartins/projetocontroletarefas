﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApiTarefas.Integration;
using WebApiTarefas.Repository;

namespace WebApiTarefas.Controllers
{
    public class HomeController : Controller
    {
        public static readonly TarefaRepository repository = new TarefaRepository();
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        [HttpPost]
        public ActionResult Index(TarefaFormModel tarefas)
        {
            repository.AddTarefa(tarefas);

            return View();
        }
    }
}
