﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiTarefas.Integration;
using WebApiTarefas.Models;
using WebApiTarefas.Repository;

namespace WebApiTarefas.Controllers
{
    public class TarefaController : ApiController
    {
        static readonly TarefaRepository repository = HomeController.repository;

        [HttpGet]                
        public string CriarTarefas()
        {
            repository.CriarTarefas();

            return "tarefas criadas com sucesso!";
        }

        [HttpGet]
        public IEnumerable<Tarefa> GetAllTarefas()
        {
            return repository.GetAll();
        }

        [HttpGet]
        public Tarefa GetTarefaByIdentificador(string identificador)
        {
            Tarefa item = repository.GetByIdentificador(identificador);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }
        [HttpGet]
        public MenorCustoEsperaResponse GetMenorCustoEspera()
        {
            return repository.GetTarefaByMenorCustoEspera();
        }
        [HttpGet]
        public MenorTempoEsperaResponse GetMenorTempoEspera()
        {
            return repository.GetTarefaByMenorTempoEspera();
        }

    }
}
