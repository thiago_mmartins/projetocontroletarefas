﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiTarefas.Integration;
using WebApiTarefas.Models;

namespace WebApiTarefas.Interfaces
{
    public interface ITarefa
    {
        Tarefa Add(Tarefa item);
        Tarefa GetByIdentificador(string identificador);
        List<Tarefa> GetAll();
        MenorTempoEsperaResponse GetTarefaByMenorTempoEspera();
        MenorCustoEsperaResponse GetTarefaByMenorCustoEspera();
    }
}
