﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiTarefas.Integration;
using WebApiTarefas.Interfaces;
using WebApiTarefas.Models;

namespace WebApiTarefas.Repository
{
    public class TarefaRepository : ITarefa
    {
        private List<Tarefa> tarefas;

        public TarefaRepository()
        {
            tarefas = new List<Tarefa>();
        }
        public void CriarTarefas()
        {
         
            Add(new Tarefa { identificador = "Refile Manual", tempo_de_execucao = 60, custo_por_hora = 30 });
            Add(new Tarefa { identificador = "Impressão", tempo_de_execucao = 30, custo_por_hora = 40 });
            Add(new Tarefa { identificador = "Acabamento", tempo_de_execucao = 40, custo_por_hora = 20 });
        }
        public Tarefa Add(Tarefa tarefa)
        {
            try
            {
         
                if (tarefa == null)
                {
                    throw new Exception("A tarefa a inserir não pode ser nula");
                }

                if(tarefas.Where(p=>p.identificador == tarefa.identificador).Count() == 0)
                    tarefas.Add(tarefa);

                return tarefa;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Tarefa AddTarefa(TarefaFormModel tarefaTela)
        {
            try
            {
                Tarefa tarefa = this.ConvertToTarefaModel(tarefaTela);

                if (tarefa == null)
                {
                    throw new Exception("A tarefa a inserir não pode ser nula");
                }

                if (tarefa.identificador.Trim() == string.Empty)
                {
                    throw new Exception("A tarefa deve conter um identificador");
                }
                           
                if (tarefas.Where(p => p.identificador == tarefa.identificador).Count() == 0)
                {
                    tarefas.Add(tarefa);
                }

                return tarefa;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public Tarefa GetByIdentificador(string identificador)
        {
            if (identificador == null || identificador.Trim() == string.Empty)
            {
                throw new Exception("Necessário informar um identificador para pesquisar!");
            }
            
            var consulta = this.tarefas.Where(p => p.identificador.Equals(identificador));
            if (!(consulta.Count() > 0))
            {
                return null;
            }

            return consulta.FirstOrDefault();
        }

        public List<Tarefa> GetAll()
        {
            return this.tarefas;
        }

        public MenorCustoEsperaResponse GetTarefaByMenorCustoEspera()
        {
            List<Tarefa> getAllTarefas = GetAll();

            var query = from tarefa in getAllTarefas
                        orderby tarefa.tempo_de_execucao
                        select tarefa.identificador;
            
            MenorCustoEsperaResponse result = new MenorCustoEsperaResponse();
            result.menor_custo_espera = query.AsEnumerable();
            return result;
        }

        public MenorTempoEsperaResponse GetTarefaByMenorTempoEspera()
        {
            List<Tarefa> getAllTarefas = GetAll();

            var query = from tarefa in getAllTarefas
                        orderby tarefa.custo_por_hora
                        select tarefa.identificador;

            MenorTempoEsperaResponse result = new MenorTempoEsperaResponse();
            result.menor_tempo_espera = query.AsEnumerable();
            return result;
        }

        public Tarefa ConvertToTarefaModel(TarefaFormModel tarefa)
        {
            Tarefa novaTarefa = new Tarefa();
            novaTarefa.custo_por_hora = tarefa.custo;
            novaTarefa.identificador = tarefa.identificador;
            novaTarefa.tempo_de_execucao = tarefa.tempo;

            return novaTarefa;
        }
    }
}