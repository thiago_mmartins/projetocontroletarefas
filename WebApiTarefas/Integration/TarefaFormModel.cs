﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTarefas.Integration
{
    public class TarefaFormModel
    {
        public string identificador{ get; set; }
        public int tempo { get; set; }
        public int custo { get; set; }
    }
}