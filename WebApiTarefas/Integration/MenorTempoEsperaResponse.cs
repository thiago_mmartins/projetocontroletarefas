﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTarefas.Models
{
    public class MenorTempoEsperaResponse
    {
        public IEnumerable<string> menor_tempo_espera { get; set; }
    }
}