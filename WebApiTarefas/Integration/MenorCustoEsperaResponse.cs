﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTarefas.Integration
{
    public class MenorCustoEsperaResponse
    {
        public IEnumerable<string> menor_custo_espera { get; set; }
    }
}